FROM node:14-alpine AS builder
WORKDIR /app
COPY package.json package-json.lock ./
RUN npm install
COPY . .
RUN npm run deploy

FROM node:14-alpine
ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV
ENV PORT 3000

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm run install && npm run cache && npm run clean 

ENV HASURA_ENDPOINT http://hasuragraphqlapi-loadbalancer-1300487192.eu-central-1.elb.amazonaws.com/v1/graphql
ENV HASURA_GRAPHQL_ADMIN_SECRET svij3jgdHZztnYbzBgmp
ENV S3_ENDPOINT blank
ENV S3_BUCKET blank
ENV S3_ACCESS_KEY_ID blank
ENV S3_SECRET_ACCESS_KEY blank

COPY --from=builder /app/dist/ dist/
COPY custom custom
COPY metadata metadata
COPY migrations migrations
COPY migrations-v1 migrations-v1

HEALTHCHECK --interval=60s --timeout=2s --retries=3 CMD wget localhost:${PORT}/healthz -q -O - > /dev/null 2>&1

EXPOSE $PORT
CMD ["npm", "run start"]
